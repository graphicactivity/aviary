<?php

return array(
    '*' => array(
        'tablePrefix' => 'craft',
        'server' => 'localhost',
    ),
    'theaviary.dev' => array(
    	'database' => 'aviary',
        'user' => 'root',
        'password' => 'xxx',
    ),
    'theaviary.co.nz' => array(
    	'database' => 'aviary',
        'user' => 'root',
        'password' => 'xxx',
    ),
);
