<?php

return array(
    '*' => array(
		'siteUrl' => null,
		'defaultWeekStartDay' => 0,
		'enableCsrfProtection' => true,
        'omitScriptNameInUrls' => true,
        'extraAllowedFileExtensions' => 'json',
        'devMode' => true,
		'cpTrigger' => 'admin',
    ),

    'theaviary.dev' => array(
        'devMode' => true,
        'environmentVariables' => array(
        	'basePath' => '/Users/GraphicActivity/Sites/aviary.dev/public/',
            'baseUrl'  => 'http://theaviary.dev',
        )
    ),

    '139.59.103.74' => array(
        'devMode' => false,
        'environmentVariables' => array(
        	'basePath' => '/var/www/public/',
            'baseUrl'  => 'http://139.59.103.74',
        )
    )
);
